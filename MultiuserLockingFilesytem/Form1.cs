﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MultiuserLockingFilesytem
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        public User GetThisUser()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Fetches the current locks on files
        /// </summary>
        public LocksFile RetrieveUserLocks()
        {
            if (!HasServerLock())
                throw new Exception("Server is locked by another user");

            throw new NotImplementedException();
        }

        public void StoreUserLocks(LocksFile locksFile)
        {
            if (!HasServerLock())
                throw new Exception("This user does not have exclusive access to the server.");

            throw new NotImplementedException();
        }

        /// <summary>
        /// Locks the locks-file on the server for any access, checks if given file can be locked, and acquires the lock, and downloads the file to given location
        /// Returns whether file was successfully checked out
        /// </summary>
        public bool CheckOutFile(ServerFile file, string targetLocation)
        {
            try
            {
                LockServer();

                LocksFile locks = RetrieveUserLocks();

                LocksFile.Item item = locks.FindLockItem(file);
                if (item.UserOwningLock != null) // File is locked!
                    return false;

                item.UserOwningLock = GetThisUser();

                StoreUserLocks(locks);



                downloadFileFromServer(file, targetLocation);



            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                UnlockServer();

            }


        }

        /// <summary>
        /// Locks the locks-file on the server for any access, checks if given file can be locked, and acquires the lock
        /// Returns whether file was successfully checked out
        /// </summary>
        public bool CheckInFile(ServerFile file, string sourceLocation)
        {
            try
            {
                LockServer();

                LocksFile locks = RetrieveUserLocks();

                LocksFile.Item item = locks.FindLockItem(file);
                if (item.UserOwningLock != GetThisUser()) // File is not owned by this user!
                    throw new Exception("You do not own the lock to this file!!!!"); //TODO: maybe remove the local file when this happens

                uploadFileToServer(file, sourceLocation);

                item.UserOwningLock = null;

                StoreUserLocks(locks);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                UnlockServer();

            }


        }


        private void downloadFileFromServer(ServerFile file, string targetFile)
        {
            throw new NotImplementedException();
        }

        private void uploadFileToServer(ServerFile file, string sourceFile)
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// Locks the server entirely. All access is forbidden for other users
        /// </summary>
        public void LockServer()
        {

        }

        /// <summary>
        /// Releases the lock on the server
        /// </summary>
        public void UnlockServer()
        {

        }

        /// <summary>
        /// Returns whether this user as an exclusive server lock
        /// </summary>
        /// <returns></returns>
        public bool HasServerLock()
        {
            throw new NotImplementedException();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }




    }
}
